class Agent
{
    private _id: number;
    private _fleet_id: number;
    private _email: string;
    constructor(id: number,fleet_id: number){
        this._id = id;
        this._fleet_id =fleet_id;
    }

    get id()
    {
        return this._id;
    }

    get fleet_id()
    {
        return this._fleet_id;
    }
}

let obj = new Agent(4,2243);
 
// console.log(obj.id);