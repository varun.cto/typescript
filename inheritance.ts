class Person{
    private _nme: string;
    private _age: number;
    
    set nme(nm: string) // for setting name
    {
        this._nme = nm;
    }
    get nme() // for getting name
    {
        return this._nme;
    }

    set age(age: number) // for setting age 
    {
        this._age = age;
    }
    get age() // for getting age
    {
        return this._age;
    }

    protected getFullDetails()
    {
        return "Age of " + this._nme + " is " + this._age;
    }
    
   
}

class Student extends Person{
    private _rollno;
    set rollno(rollno: number)
    {
        this._rollno = rollno;
    }

    get rollno()
    {
        return this._rollno;
    }

     getDetails()
    {
        return this.getFullDetails();
    }
}

class Teacher extends Person{
    private _salary;
    set salary(sl: number)
    {
        this._salary = sl;
    }

    get salary()
    {
        return this._salary;
    }

    
}

let std = new Student();
std.age = 20;
std.nme = "Varun";
std.rollno = 1102;
let tch = new Teacher();
tch.age = 25;
tch.nme = "Gurpreet";
tch.salary = -55000;
// console.log(std.rollno);
// console.log(tch.salary);
console.log(std.getDetails());

