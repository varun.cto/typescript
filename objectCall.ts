class Animal{
    private nme;
    constructor(nm?: string)
    {
        if(nm == undefined){
            this.nme = "Default name";
        }else{
            this.nme = nm;            
        }
    }

    private showName(){
        return this.nme;
    }

    public checkMe(){
        this.showName();
    }
}

let cow = new Animal();
console.log(cow.checkMe());