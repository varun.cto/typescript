var Agent = (function () {
    function Agent() {
        this._id = 10;
        console.log("Constructor called");
    }
    Object.defineProperty(Agent.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    return Agent;
}());
var obj = new Agent();
// console.log(obj.id); 
